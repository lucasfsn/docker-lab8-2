const express = require('express');
const redis = require('redis');

const app = express();
const client = redis.createClient({
  legacyMode: true,
  socket: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
  },
});

app.use(express.json());

(async () => {
  try {
    await client.connect();
    console.log('Connected to Redis');
  } catch (err) {
    console.log('Failed to connect to Redis');
  }
})();

app.post('/message', (req, res) => {
  const { key, message } = req.body;

  client.set(key, message);

  res.send('Message saved');
});

app.get('/message/:key', (req, res) => {
  const { key } = req.params;

  client.get(key, (err, value) => {
    if (err) {
      res.send('Error occurred');
      return;
    }

    res.send(value);
  });
});

app.listen(3001, () => {
  console.log(`Server is running on port 3001`);
});
